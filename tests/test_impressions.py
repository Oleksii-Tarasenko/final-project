import http
import json
from dataclasses import dataclass
from unittest.mock import patch

from src import app


@dataclass
class FakeImpression:
    """ Impression Fake class for testing with out access to db. """
    description = "fake descr"
    duration = 1.0
    price = 100.0
    executor = "fake executor"
    place = "fake place"
    title = "Fake title"


class TestImpressions:
    uuid = []

    def test_get_impressions_with_db(self):
        """ Test to get impressions from db. """
        client = app.test_client()
        resp = client.get('/api/impressions')
        assert resp.status_code == http.HTTPStatus.OK

    @patch('src.services.impression_service.ImpressionService.fetch_all_impressions')
    def test_get_impressions_mock_db(self, mock_db_call):
        """ Test imitating getting impressions, with out access to db. """
        client = app.test_client()
        resp = client.get('/api/impressions')

        mock_db_call.assert_called_once()
        assert resp.status_code == http.HTTPStatus.OK
        assert len(resp.json) == 0

    def test_create_impression_with_db(self):
        """ Test creating impression in db. """
        client = app.test_client()
        data = {
            "description": "Massage to relax",
            "duration": 1.5,
            "price": 1000.0,
            "executor": "Massage Rooms",
            "place": "Kiev, Lva Tolstogo square",
            "title": "Massage"
        }
        resp = client.post('/api/impressions', data=json.dumps(data), content_type='application/json')
        assert resp.status_code == http.HTTPStatus.CREATED
        assert resp.json['title'] == "Massage"

        self.uuid.append(resp.json['uuid'])

    def test_create_impression_with_mock_db(self):
        """ Test imitation creating impression, with out access to db. """
        with patch('src.db.session.add', autospec=True) as mock_session_add, \
                patch('src.db.session.commit', autospec=True) as mock_session_commit:
            client = app.test_client()
            data = {
                "description": "Test",
                "duration": 1,
                "price": 100.0,
                "executor": "test test",
                "place": "local",
                "title": "TEST"
            }
            resp = client.post('/api/impressions', data=json.dumps(data), content_type='application/json')
            mock_session_add.assert_called_once()
            mock_session_commit.assert_called_once()

    def test_update_impression_with_db(self):
        """ Test updating impression in db"""
        client = app.test_client()
        url = f'/api/impressions/{self.uuid[0]}'
        data = {
            "description": "TEST Massage to relax",
            "executor": "TEST Massage Rooms",
            "title": "TEST Massage",
            "place": "local"
        }
        resp = client.put(url, data=json.dumps(data), content_type='application/json')
        assert resp.status_code == http.HTTPStatus.OK
        assert resp.json['title'] == "TEST Massage"

    def test_update_impression_with_mock_db(self):
        """ Test imitation updating impression, with out access to db. """
        with patch('src.services.impression_service.ImpressionService.fetch_impression_by_uuid') as mocked_query, \
                patch('src.db.session.add', autospec=True) as mock_session_add, \
                patch('src.db.session.commit', autospec=True) as mock_session_commit:
            mocked_query.return_value = FakeImpression()
            client = app.test_client()
            url = f'/api/impressions/1'
            data = {
                "description": "update test for fake impr",
                "executor": "update fake executor",
                "title": "Update fake impr for mock test",
                "place": "update fake place"
            }
            resp = client.put(url, data=json.dumps(data), content_type='application/json')
            mock_session_add.assert_called_once()
            mock_session_commit.assert_called_once()

    def test_delete_impression_with_db(self):
        """ Test deleting impression in db. """
        client = app.test_client()
        url = f'/api/impressions/{self.uuid[0]}'
        resp = client.delete(url)
        assert resp.status_code == http.HTTPStatus.NO_CONTENT
