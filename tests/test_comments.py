import http
import json
from dataclasses import dataclass
from unittest.mock import patch

from src import app


@dataclass
class FakeComment:
    """ Comment Fake class for testing with out access to db. """
    name = "fake name"
    rating = 1.0
    description = "fake description"
    impression_id = 0


class TestComments:
    uuid = []

    def test_get_comments_with_db(self):
        """ Test to get comments from db. """
        client = app.test_client()
        resp = client.get('/api/comments')
        assert resp.status_code == http.HTTPStatus.OK

    @patch('src.services.comment_service.CommentService.fetch_all_comments')
    def test_get_comments_mock_db(self, mock_db_call):
        """ Test imitating getting comments, with out access to db. """
        client = app.test_client()
        resp = client.get('/api/comments')

        mock_db_call.assert_called_once()
        assert resp.status_code == http.HTTPStatus.OK
        assert len(resp.json) == 0

    def test_create_comment_with_db(self):
        """ Test creating comment in db. """
        client = app.test_client()
        data = {
            "description": "fake good",
            "rating": 2.0,
            "name": "test name",
            "impression_id": 0
        }
        resp = client.post('/api/comments', data=json.dumps(data), content_type='application/json')
        assert resp.status_code == http.HTTPStatus.CREATED
        assert resp.json['name'] == "test name"

        self.uuid.append(resp.json['uuid'])

    def test_create_comment_with_mock_db(self):
        """ Test imitation creating comment, with out access to db. """
        with patch('src.db.session.add', autospec=True) as mock_session_add, \
                patch('src.db.session.commit', autospec=True) as mock_session_commit:
            client = app.test_client()
            data = {
                "description": "fake good 2",
                "rating": 2.2,
                "name": "test name 2",
                "impression_id": 0
            }
            resp = client.post('/api/comments', data=json.dumps(data), content_type='application/json')
            mock_session_add.assert_called_once()
            mock_session_commit.assert_called_once()

    def test_update_comment_with_db(self):
        """ Test updating comment in db"""
        client = app.test_client()
        url = f'/api/comments/{self.uuid[0]}'
        data = {
            "description": "test update",
            "rating": 3.0,
            "name": "test update",
            "impression_id": 1
        }
        resp = client.put(url, data=json.dumps(data), content_type='application/json')
        assert resp.status_code == http.HTTPStatus.OK
        assert resp.json['name'] == "test update"

    def test_update_comment_with_mock_db(self):
        """ Test imitation updating comment, with out access to db. """
        with patch('src.services.comment_service.CommentService.fetch_comment_by_uuid') as mocked_query, \
                patch('src.db.session.add', autospec=True) as mock_session_add, \
                patch('src.db.session.commit', autospec=True) as mock_session_commit:
            mocked_query.return_value = FakeComment()
            client = app.test_client()
            url = f'/api/comments/1'
            data = {
                "description": "test update with mock db",
                "rating": 3.0,
                "name": "test update with mock db",
                "impression_id": 1
            }
            resp = client.put(url, data=json.dumps(data), content_type='application/json')
            mock_session_add.assert_called_once()
            mock_session_commit.assert_called_once()

    def test_delete_comment_with_db(self):
        """ Test deleting impression in db. """
        client = app.test_client()
        url = f'/api/comments/{self.uuid[0]}'
        resp = client.delete(url)
        assert resp.status_code == http.HTTPStatus.NO_CONTENT
