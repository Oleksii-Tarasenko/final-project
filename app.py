from flask import redirect

from src import app
import os


@app.route('/')
def start():
    return redirect('/impressions')

def server():
    port = int(os.environ.get('PORT', 8000))
    app.run(debug=True, host='0.0.0.0')

if __name__ == '__main__':
    server()
