from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, IntegerField, FloatField, validators
from wtforms.validators import DataRequired


class ImpressionForm(FlaskForm):
    """"Impression Form to collect data from the template"""
    class Meta:
        csrf = False

    title = StringField("Title: ", validators=[DataRequired()])
    executor = StringField("Executor: ", validators=[DataRequired()])
    description = TextAreaField("Description:", validators=[DataRequired()])
    place = TextAreaField("Place:", validators=[DataRequired()])
    duration = IntegerField("Duration:", validators=[DataRequired()])
    price = IntegerField("Price:", validators=[DataRequired()])
    submit = SubmitField("Submit")


class CommentForm(FlaskForm):
    """"Comment Form to collect data from the template"""
    class Meta:
        csrf = False

    name = StringField("Name: ", validators=[DataRequired()])
    description = TextAreaField("Description:", validators=[DataRequired()])
    rating = FloatField("Rating:", validators=[DataRequired(), validators.NumberRange(min=0, max=10)])
    impression_id = IntegerField()
    submit = SubmitField("Submit")
