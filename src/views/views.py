from flask import render_template, request, make_response, redirect

import requests

from src import api, app
from src.resources.aggregations import CommentRatingApi
from src.resources.comments import CommentListApi
from src.resources.impressions import ImpressionListApi
from src.views.forms import CommentForm, ImpressionForm

api.add_resource(ImpressionListApi, '/api/impressions', '/api/impressions/<uuid>', strict_slashes=False)
api.add_resource(CommentListApi, '/api/comments', '/api/comments/<uuid>', '/api/comments/impression/<impression_id>',
                 strict_slashes=False)
# api.add_resource(AggregationApi, '/api/aggregations', strict_slashes=False)
api.add_resource(CommentRatingApi, '/api/comments-rating/', '/api/comments-rating/<impression_id>',
                 strict_slashes=False)


@app.route('/impressions')
def impressions():
    """Impressions list. Access using request, imitation js/jquery & other clients requests to the Api throw http."""
    # impressions = requests.get(f'http://{request.host}/api/impressions/').json()
    impressions = requests.request('GET', f'http://{request.host}/api/impressions/').json()
    comments_ratings = requests.request('GET', f'http://{request.host}/api/comments-rating/').json()
    return render_template('impressions.html', title='Impressions', impressions=impressions,
                           comments_ratings=comments_ratings)


"""
@app.route('/impressions')
def impressions():
    #Impressions list. Alternative method with access to Api without routing  
    listApi = ImpressionListApi()
    impressions, _ = listApi.get()
    return render_template('impressions.html', title='Impressions', impressions=impressions)
"""


@app.route('/impressions/<uuid>')
def impression_details(uuid):
    """Impression details. Access using request, imitation js/jquery & other clients requests to the Api throw http."""
    # req = requests.get(f'http://{request.host}/api/impressions/{uuid}')
    impression = requests.request('GET', f'http://{request.host}/api/impressions/{uuid}').json()
    impression_id = impression['id']
    com_quant = len(requests.request('GET', f'http://{request.host}/api/comments/impression/{impression_id}').json())
    return render_template('impression.html', impression=impression, com_quant=com_quant, title='Impression details')


"""
@app.route('/impressions/<uuid>')
def impression_details(uuid):
    #Impression details. Alternative method with access to Api without routing
    listApi = ImpressionListApi()
    impression = listApi.get(uuid=uuid)[0]
    return render_template('impression.html', title=impression['title'], impression=impression)
"""


@app.route('/impressions/new', methods=['GET'])
def impression_add_form():
    """Creating new Impression."""
    form = ImpressionForm()
    return render_template('impression-add-form.html', form=form, title='Add new impression')


@app.route('/impressions/new-save', methods=['POST'])
def impression_new_save():
    """Saving new Impression.Access using request,imitation js/jquery & other clients requests to the Api throw http."""
    form = ImpressionForm(request.form)
    if not form.validate():
        return make_response(
            '<h2>Entered data is incorrect. Duration and Price fields should be filled by numeric values.'
            ' Please, enter correct values.</h2>', 404)
    else:
        data = form.data
        data.pop('submit', None)
        requests.request('POST', f'http://{request.host}/api/impressions/', json=data)
    return redirect('/impressions')


"""
@app.route('/impressions/new-save', methods=['POST'])
def impression_new_save():
    #Saving new Impression. Alternative method with access to Api without routing
    form = ImpressionForm(request.form)
    if not form.validate():
        return make_response('', 404)
    else:
        data = form.data
        data.pop('submit', None)
        ImpressionListApi().post(data)
    return redirect('/impressions')
"""


@app.route('/impressions/edit/<uuid>', methods=['GET'])
def impression_edit_form(uuid):
    """Editing Impression. Access using request, imitation js/jquery & other clients requests to the Api throw http."""
    req = requests.request('GET', f'http://{request.host}/api/impressions/{uuid}')
    form = ImpressionForm()
    return render_template('impression-edit-form.html', impression=req.json(), form=form, uuid=uuid,
                           title='Edit impression')


@app.route('/impressions/edited-save/<uuid>', methods=['POST'])
def impression_edit_save(uuid):
    """Saving edited Impression.Access using request,imitation js/jquery&other clients requests to the Api throw http"""
    form = ImpressionForm(request.form)
    if not form.validate():
        return make_response(
            '<h2>Entered data is incorrect. Duration and Price fields should be filled by numeric values.'
            ' Please, enter correct values.</h2>', 404)
    else:
        data = form.data
        data.pop('submit', None)
        requests.request('PUT', f'http://{request.host}/api/impressions/{uuid}', json=data)
    return redirect('/impressions')


@app.route('/impressions/delete/<uuid>')
def impression_delete(uuid):
    """Deleting Impression. Access using request, imitation js/jquery & other clients requests to the Api throw http."""
    requests.request('DELETE', f'http://{request.host}/api/impressions/{uuid}')
    return redirect('/impressions')


"""
@app.route('/impressions/delete/<uuid>', methods=['GET', 'POST'])
def impression_delete(uuid):
    #Deleting Impression. Alternative method with access to Api without routing  
    impression = ImpressionListApi()
    impression.delete(uuid)
    return redirect('/impressions')
"""


@app.route('/comments/impression/<impression_id>')
def comments(impression_id):
    """ Comments list for some Impression.
    Access using request, imitation js/jquery & other clients requests to the Api throw http."""
    req = requests.request('GET', f'http://{request.host}/api/comments/impression/{impression_id}')
    return render_template('comments.html', comments=req.json(), impression_id=impression_id, com_quant=len(req.json()),
                           title='Comments')


@app.route('/comments/new/<impression_id>', methods=['GET'])
def comment_add_form(impression_id):
    """Creating new Comment for some Impression."""
    form = CommentForm()
    return render_template('comment-add-form.html', form=form, impression_id=impression_id, title='Add new comment')


@app.route('/comments/new-save/<impression_id>', methods=['POST'])
def comment_new_save(impression_id):
    """Saving new Impression.Access using request, imitation js/jquery&other clients requests to the Api throw http."""
    form = CommentForm(request.form)
    if not form.validate():
        return make_response(
            '<h2>Entered data is incorrect. Rating should be in range from 0 to 10. Please, enter correct values.</h2>',
            404)
    else:
        data = form.data
        data.pop('submit', None)
        data.update({'impression_id': impression_id})
        requests.request('POST', f'http://{request.host}/api/comments/', json=data)
    return redirect(f'/comments/impression/{impression_id}')


@app.route('/comments/edit/<uuid>', methods=['GET'])
def comment_edit_form(uuid):
    """Editing Comment. Access using request, imitation js/jquery & other clients requests to the Api throw http."""
    req = requests.request('GET', f'http://{request.host}/api/comments/{uuid}')
    form = CommentForm()
    return render_template('comment-edit-form.html', comment=req.json(), form=form, uuid=uuid, title='Edit comment')


@app.route('/comments/edited-save/<uuid>', methods=['POST'])
def comments_edit_save(uuid):
    """Saving edited Comment.Access using request, imitation js/jquery&other clients requests to the Api throw http."""
    req = requests.request('GET', f'http://{request.host}/api/comments/{uuid}')
    impression_id = req.json().get('impression_id')
    form = CommentForm(request.form)
    if not form.validate():
        return make_response(
            '<h2>Entered data is incorrect. Rating should be in range from 0 to 10. Please, enter correct values.</h2>',
            404)
    else:
        data = form.data
        data.pop('submit', None)
        data.update({'impression_id': impression_id})
        requests.request('PUT', f'http://{request.host}/api/comments/{uuid}', json=data)
    return redirect(f'/comments/impression/{impression_id}')


@app.route('/comments/delete/<uuid>')
def comment_delete(uuid):
    """ Deleting Comment. Access using request, imitation js/jquery & other clients requests to the Api throw http."""
    req = requests.request('GET', f'http://{request.host}/api/comments/{uuid}')
    impression_id = req.json().get('impression_id')
    requests.request('DELETE', f'http://{request.host}/api/comments/{uuid}')
    return redirect(f'/comments/impression/{impression_id}')
