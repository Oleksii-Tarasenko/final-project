from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from marshmallow_sqlalchemy.fields import Nested

from src.database.models import Comment


class CommentSchema(SQLAlchemyAutoSchema):
    """Comment Schema"""
    class Meta:
        model = Comment
        load_instance = True
        include_fk = True
    #impressions = Nested('ImpressionSchema', exclude=('comments', ))
