from src.database.models import Comment


class CommentService:
    """ Resource Controller to work with database for Comment model"""
    @staticmethod
    def fetch_all_comments(session):
        """ Get all comments """
        return session.query(Comment)

    @classmethod
    def fetch_comment_by_uuid(cls, session, uuid):
        """ Get all comment by uuid """
        return cls.fetch_all_comments(session).filter_by(uuid=uuid).first()

    @classmethod
    def fetch_comment_by_impression_id(cls, session, impression_id):
        """ Get all comments by impression_id """
        return cls.fetch_all_comments(session).filter_by(impression_id=impression_id)

