from src.database.models import Impression


class ImpressionService:
    """ Resource Controller to work with database for Impression model"""

    @staticmethod
    def fetch_all_impressions(session):
        """ Get all impressions """
        return session.query(Impression)

    @classmethod
    def fetch_impression_by_uuid(cls, session, uuid):
        """ Get all impression by uuid """
        return cls.fetch_all_impressions(session).filter_by(uuid=uuid).first()
