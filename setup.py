import os
from setuptools import setup, find_packages


def read(f):
    return open(os.path.join(os.path.dirname(__file__), f)).read().strip().split('\n')


install_requires = read('requirements.txt')

extras_require = {}

setup(
    name="Impressions app",
    version='0.0.1',
    classifiers=[
        'Programming language :: Python :: 3.8 ',
        'Topic :: Internet :: WWW/HTTP'
    ],
    author="Alex",
    license='proprietary',
    packages=find_packages(exclude=['tests', 'venv', 'static', 'data', 'migrations']),
    install_requires=install_requires,
    include_package_data=True,
    extras_require=extras_require,
    entry_points={
        "console_scripts": [
            "server = app:server",
        ]
    }
)
