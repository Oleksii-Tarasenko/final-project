"Impressions App" is a Web Application which is developed to show and edit a list of impressions
and comments for each of it.


Web Application is deployed on Heroku, it and it's APi are public accessible:

Web Application - http://impressions-api-app.herokuapp.com/

- for all impressions - https://impressions-api-app.herokuapp.com/api/impressions
- for exact impression - https://impressions-api-app.herokuapp.com/api/impressions/<uuid>
- for all comments - https://impressions-api-app.herokuapp.com/api/comments
- for exact comment - https://impressions-api-app.herokuapp.com/api/comments/<uuid>
- for all comments which belongs to some impression
     - https://impressions-api-app.herokuapp.com/api/comments/impression/<impression_id>
- for average rating's of impressions - https://impressions-api-app.herokuapp.com/api/comments-rating/
- for exact impression's rating - https://impressions-api-app.herokuapp.com/api/comments-rating/<impression_id>
- to get and edit impressions swagger-blueprint can be used: http://impressions-api-app.herokuapp.com/swagger/
(in case of 'POST' or 'PUT' actions field 'rating' should be deleted manually)


Travis CI build status and Coveralls coverage can be checked by Pipeline Icon('green arrow', push on it for details)
on https://gitlab.com/Oleksii-Tarasenko/final-project).
Pylint report can be found in repository('pylint_report.txt') or in 'Job log' of the latest Travis build
(push on 'Travis CI build status' in the latest pipeline on git-lab).


Also Web application can be deployed locally(instruction below).

Web Application - localhost:5000/

Web Api, which can be accessed and edited:
- for all impressions - localhost:5000/api/impressions
- for exact impression - localhost:5000/api/impressions/<uuid>
- for all comments - localhost:5000/api/comments
- for exact comment - localhost:5000/api/comments/<uuid>
- for all comments which belongs to some impression - localhost:5000/api/comments/impression/<impression_id>
- for average rating's of impressions - localhost:5000/api/comments-rating/
- for exact impression's rating - localhost:5000/api/comments-rating/<impression_id>
- to get and edit impressions swagger-blueprint can be used: localhost:5000/swagger/
(in case of 'POST' or 'PUT' actions field 'rating' should be deleted manually)

## Sequence of actions for starting a project locally
1. Linux: normally under Linux you should alreday have installed python, so just pass this step.
   Windows: Pass this step if you already have python installed, to check this open CMD and type 'python --version'. 
   If not, -  download from [official site] (https://www.python.org/downloads/) and install ** Python interpreter 3.8
   or higher.

2. Pass this step if you already have Git installed. Download and install the [Git Bash] package tool
    (https://git-scm.com/downloads).

3. Open the Terminal and create a folder for the project('mkdir project_name') and open it('cd project_name').

4. Create new virtual enviroment for the project (Linux:'sudo apt install python3.8 python3.8-venv python3-venv',
    'python3.8 -m venv venv_name'; Windows: 'virtualenv venv_name').

5. Activate venv(Linux: 'source venv_name/bin/activate', Windows: 'venv_name\Scripts\activate.bat').
    Check python's version in your venv('python --version'), it should be 3.8 or higher.

6. Clone project('git clone https://gitlab.com/Oleksii-Tarasenko/final-project.git')

7. Open final-project directory('cd final-project') and execute the setup.py('python setup.py develop')

8. Optional step: you can use 'pytest' to run tests.

9. Run a server with 'server'(Debug Mode, logging info will be displayed in terminal) or run it under Gunicorn with
    'gunicorn -w 5 --access-logfile file-name --bind 0.0.0.0:5000 app:app'(only for Linux, logging info will be writen
     in file 'file-name' in project's directory).

10. Open a browser and go to http://localhost:5000/ to take a look at Web App and urls mentioned above to check the
    Web Api.

11. After these you can stop the server(Press CTRL+C to quit) and deactivate virtual enviroment('cd..';
    Linux: 'deactivate'; Windows: 'venv_name\Scripts\deactivate.bat')

All descibed actions were tested on Linux Ubuntu 18.04 and Windows 10, on other Operation Systems described action's
output is not guaranteed.
